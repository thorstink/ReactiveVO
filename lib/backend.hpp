#ifndef PI_PROCESSOR_BACKEND_H_
#define PI_PROCESSOR_BACKEND_H_

#include <memory>
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/slam/expressions.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/ExpressionFactorGraph.h>
#include <gtsam/inference/Symbol.h>
#include <imudriver.hpp>

using namespace gtsam;

using symbol_shorthand::X; // Pose3 (x,y,z,r,p,y)
using symbol_shorthand::V; // Vel   (xdot,ydot,zdot)
using symbol_shorthand::B; // Bias  (ax,ay,az,gx,gy,gz)

class backend {
public:
	backend() : initialized_(false), correction_count_(0) {};
	void initialize(const drivers::imuMsg&);
	bool initialized() { return initialized_; }
    void imuCallback(const drivers::imuMsg& _msg);
    void rgbdCallback(const Eigen::Matrix4d& _T);
private:
    std::unique_ptr<ExpressionFactorGraph> factorGraph_;
    PreintegrationType *imu_preintegrated_;
    std::unique_ptr<Values> initial_values_;

    noiseModel::Diagonal::shared_ptr pose_noise_model_;
	noiseModel::Diagonal::shared_ptr velocity_noise_model_;
	noiseModel::Diagonal::shared_ptr bias_noise_model_;

	boost::shared_ptr<PreintegratedCombinedMeasurements::Params> p_;

	// Store previous state for the imu integration and the latest predicted outcome.
	NavState prev_state_;
	NavState prop_state_;
	imuBias::ConstantBias prev_bias_;
	uint64_t previous_timestamp_;

    bool initialized_;
	int correction_count_;

    void optimize();

    std::chrono::high_resolution_clock m_clock;
    uint64_t microseconds() 
	{
		return std::chrono::duration_cast<std::chrono::microseconds>(m_clock.now().time_since_epoch()).count();
	}
};

#endif  // PI_PROCESSOR_BACKEND_H_