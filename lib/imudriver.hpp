#ifndef IMU_H_
#define IMU_H_

#include "serial/serial.h"
#include <range/v3/core.hpp>
#include <range/v3/view/split.hpp>
#include <iostream>

namespace drivers {

class imuMsg {
public:
	uint64_t t = 0;
	double ax = 0;
	double ay = 0;
	double az = 0;
	double gx = 0;
	double gy = 0;
	double gz = 0;

	// For some RxCPP stuff there needs to be a compare-operator, so we implement one.
	// https://github.com/ReactiveX/RxCpp/issues/420
    bool operator==(const imuMsg& a) const
    {
        return(a.t == t);
    }
};

class imuDriver
{
public:
	imuDriver() 
	{
		std::string port("/dev/ttyUSB1");

		try
		{
		    ser_.setPort(port);
		    ser_.setBaudrate(115200);
		    serial::Timeout to = serial::Timeout::simpleTimeout(1000);
		    ser_.setTimeout(to);
		    ser_.open();
		}
		catch (serial::IOException& e)
		{
		    std::cout << "Unable to open port " << port << "\n";
		}

		if(ser_.isOpen()){
		    std::cout << "Serial Port initialized\n";
		}
	}
	~imuDriver() {};

	bool getMeasurement(imuMsg& _msg)
	{
		std::string measurement = ser_.readline(255, "\n");

		// Fancy string-split using ranges-v3		
		std::vector<std::string> splits = ranges::view::split(measurement, 
			[] (const auto c) {
			 return c == ',';
		});

		try {
			_msg.t = std::stoull(splits[0]);
		}
		catch(std::invalid_argument& e){
			std::cout << "timestamp is invalid"	<< std::endl;
			return false;
		}
		catch(std::out_of_range& e){
			std::cout << "timestamp overflows" << std::endl;
			return false;
		}

		try {
			_msg.ax = std::stod(splits[1]);
		}
		catch(std::invalid_argument& e){
			std::cout << "ax value is invalid, ax = " << splits[1] << std::endl;
			return false;
		}

		try {
			_msg.ay = std::stod(splits[2]);
		}
		catch(std::invalid_argument& e){
			std::cout << "ay value is invalid, ay = " << splits[2] << std::endl;
			return false;
		}

		try {
			_msg.az = std::stod(splits[3]);
		}
		catch(std::invalid_argument& e){
			std::cout << "az value is invalid, az = " << splits[3] << std::endl;
			return false;
		}

		try {
			_msg.gx = std::stod(splits[4]);
		}
		catch(std::invalid_argument& e){
			std::cout << "gx value is invalid, gx = " << splits[4] << std::endl;
			return false;
		}

		try {
			_msg.gy = std::stod(splits[5]);
		}
		catch(std::invalid_argument& e){
			std::cout << "gy value is invalid, gy = " << splits[5] << std::endl;
			return false;
		}

		try {
			_msg.gz = std::stod(splits[6]);
		}
		catch(std::invalid_argument& e){
			std::cout << "gz value is invalid, gz = " << splits[6] << std::endl;
			return false;
		}

		return true;
	}
	serial::Serial ser_;
	
};
}
#endif