#ifndef REALSENSEDRIVER_H_
#define REALSENSEDRIVER_H_

#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp> // Include OpenCV API
#include <iostream>

namespace drivers {

class realsenseDriver
{
public:
	realsenseDriver() {

		auto list = ctx.query_devices(); // Get a snapshot of currently connected devices
		if (list.size() == 0) 
		    throw std::runtime_error("No device detected. Is it plugged in?");
		dev = list.front();

		rs2::config cfg;
		cfg.enable_stream(RS2_STREAM_DEPTH, 848, 480, RS2_FORMAT_Z16, 30);

		pipe.start(cfg);

		for(int i = 0; i < 10; i++)
		{
			//Wait for all configured streams to produce a frame
			frames 				= pipe.wait_for_frames();
			auto depth 			= frames.get_depth_frame();
			// reference_depth 	= depth_frame_to_meters(pipe, depth);
		}
		std::cout << "camera ready" << std::endl;
	};
	~realsenseDriver() {};

	rs2::context ctx;
	rs2::device dev;
    rs2::pipeline pipe;
    rs2::frameset frames;
};
}
#endif