#ifndef PI_PROCESSOR_POINT_REGISTRATION_H_
#define PI_PROCESSOR_POINT_REGISTRATION_H_

#include <msgs_lib/depth.pb.h>
#include <opencv2/highgui.hpp>
#include <nicp/imageutils.h>
#include <nicp/pinholepointprojector.h>
#include <nicp/depthimageconverterintegralimage.h>
#include <nicp/statscalculatorintegralimage.h>
#include <nicp/alignerprojective.h>

// Convert rs2::frame to cv::Mat
cv::Mat frame_to_mat(const rs2::frame& f)
{
    using namespace cv;
    using namespace rs2;

    auto vf = f.as<video_frame>();
    const int w = vf.get_width();
    const int h = vf.get_height();

    if (f.get_profile().format() == RS2_FORMAT_BGR8)
    {
        return Mat(Size(w, h), CV_8UC3, (void*)f.get_data(), Mat::AUTO_STEP);
    }
    else if (f.get_profile().format() == RS2_FORMAT_RGB8)
    {
        auto r = Mat(Size(w, h), CV_8UC3, (void*)f.get_data(), Mat::AUTO_STEP);
        cvtColor(r, r, CV_RGB2BGR);
        return r;
    }
    else if (f.get_profile().format() == RS2_FORMAT_Z16)
    {
        return Mat(Size(w, h), CV_16UC1, (void*)f.get_data(), Mat::AUTO_STEP);
    }
    else if (f.get_profile().format() == RS2_FORMAT_Y8)
    {
        return Mat(Size(w, h), CV_8UC1, (void*)f.get_data(), Mat::AUTO_STEP);
    }

    throw std::runtime_error("Frame format is not supported yet!");
}

// Converts depth frame to a matrix of doubles with distances in meters
cv::Mat depth_frame_to_meters(const rs2::pipeline& pipe, const rs2::depth_frame& f)
{
    using namespace cv;
    using namespace rs2;

    Mat dm = frame_to_mat(f);
    dm.convertTo(dm, CV_64F);
    auto depth_scale = pipe.get_active_profile()
        .get_device()
        .first<depth_sensor>()
        .get_depth_scale();
    dm = dm * depth_scale;
    return dm;
}

class matcher {
	public:
		matcher() {
			float imageScaling = 1;
			Eigen::Matrix3f cameraMatrix;
			cameraMatrix <<
			430.51f,   0.0f, 425.805f,
			  0.0f, 430.51f, 248.872f,
			  0.0f,   0.0f,   1.0f;

			// Create the PinholepointProjector_
			pointProjector_.setMinDistance(0.2f);
			pointProjector_.setMaxDistance(10.0f);
			pointProjector_.setCameraMatrix(cameraMatrix);

			pointProjector_.setImageSize(848, 480);
			pointProjector_.setTransform(Eigen::Isometry3f::Identity());
			pointProjector_.scale(1.0f / imageScaling);

			statsCalculator_.setMinImageRadius(20 / imageScaling);
			statsCalculator_.setMaxImageRadius(40 / imageScaling);
			statsCalculator_.setMinPoints(16 / imageScaling);
			statsCalculator_.setCurvatureThreshold(0.2f);
			statsCalculator_.setWorldRadius(0.1f);    

			pointInformationMatrixCalculator_.setCurvatureThreshold(0.02f);
			normalInformationMatrixCalculator_.setCurvatureThreshold(0.02f);

			// Create DepthImageConverter
			converter_  = new nicp::DepthImageConverterIntegralImage(&pointProjector_, &statsCalculator_,
			          &pointInformationMatrixCalculator_,
			          &normalInformationMatrixCalculator_);
			
			// Create CorrespondenceFinder
			correspondenceFinder_.setImageSize(pointProjector_.imageRows(), pointProjector_.imageCols());
			correspondenceFinder_.setInlierDistanceThreshold(0.5f);
			correspondenceFinder_.setInlierNormalAngularThreshold(0.95f);
			correspondenceFinder_.setFlatCurvatureThreshold(0.02f);
			// correspondenceFinder_.setDemotedToGICP(true);

			linearizer_.setInlierMaxChi2(9e3);
			linearizer_.setRobustKernel(true);
			linearizer_.setZScaling(true);    
			linearizer_.setAligner(&aligner_);

			aligner_.setOuterIterations(10);    
			aligner_.setLambda(1e3);
			aligner_.setProjector(&pointProjector_);
			aligner_.setCorrespondenceFinder(&correspondenceFinder_);
			aligner_.setLinearizer(&linearizer_);  



		}

		Eigen::Isometry3f match(const nicp::DepthImage& referenceDepthImage, const nicp::DepthImage& inputDepthImage)
		{

			// Get clouds from depth images
			Eigen::Isometry3f initialGuess = Eigen::Isometry3f::Identity();
			Eigen::Isometry3f sensorOffset = Eigen::Isometry3f::Identity();

			// nicp::RawDepthImage rawDepth;
			nicp::Cloud referenceCloud, currentCloud;

			converter_->compute(referenceCloud, referenceDepthImage);
			converter_->compute(currentCloud, inputDepthImage);

			// Perform the registration
			aligner_.setReferenceCloud(&referenceCloud);
			aligner_.setCurrentCloud(&currentCloud);
			aligner_.setInitialGuess(initialGuess);
			aligner_.setSensorOffset(sensorOffset);
			aligner_.align();  

			// // Write the result
			Eigen::Isometry3f T = aligner_.T();

			return T;
		}

	private:
		nicp::PinholePointProjector 			pointProjector_;
		nicp::StatsCalculatorIntegralImage 		statsCalculator_;    
		nicp::PointInformationMatrixCalculator 	pointInformationMatrixCalculator_;
		nicp::NormalInformationMatrixCalculator normalInformationMatrixCalculator_;
		nicp::DepthImageConverterIntegralImage *converter_;
		nicp::CorrespondenceFinderProjective 	correspondenceFinder_;

		// Create Linearizer and Aligner
		nicp::Linearizer linearizer_;
		nicp::AlignerProjective aligner_;

};

#endif