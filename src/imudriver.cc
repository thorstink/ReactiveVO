#include <chrono>
#include <thread>
#include <Eigen/Dense>
#include <Eigen/Geometry>

// hardware
#include <imudriver.hpp>
#include <realsensedriver.hpp>

// processing
#include <backend.hpp>
#include <point_registration.h>
#include <opencv2/opencv.hpp> // Include OpenCV API

// Middleware libraries
#include <ros/ros.h>
#include <rxcpp/rx.hpp>
#include <sensor_msgs/Imu.h>

// create alias' to simplify code
// these are owned by the user so that
// conflicts can be managed by the user.
namespace rx=rxcpp;
namespace rxsub=rxcpp::subjects;
namespace rxu=rxcpp::util;


//
// montanare 
// valeghi 9rechts, voor bruggetje
// asfalt -> grindweg (300/400 meter veld met bomen)
// paal: biels trein placket -> links 

// http://flames-of-code.netlify.com/blog/rxcpp-copy-operator/

int main(int argc, char **argv)
{
	ros::init(argc, argv, "VO_application_node");
	rx::schedulers::run_loop rl;

	auto mainthread 	= rx::observe_on_run_loop(rl);
	auto icpobsthread 	= rx::synchronize_new_thread();
	auto graphthread 	= rx::synchronize_new_thread();
	auto icpthread 		= rx::synchronize_new_thread();
	auto imuthread 		= rx::synchronize_new_thread();

	backend backendProcessor;
    matcher matchObj;

	// Declare RealSense pipeline, encapsulating the actual device and sensors
	rs2::pipeline pipe;

	rxsub::subject<drivers::imuMsg> 	imu;						// imu measurement stream
	rxsub::subject<rs2::depth_frame> 	rgbdcamera;	// rgbd measurement stream
	rxsub::subject<rs2::depth_frame> 	nicp;			// icp-variant that outputs a 3d isometry transformation
	rxsub::subject<Eigen::Isometry3f> 	current_pose;		

    // Bundles the keyframe-measurement with the most recent imu-measurement
	auto initstream = rgbdcamera.get_observable().with_latest_from(imu.get_observable());
    
    initstream
        .take_while([&](std::tuple<rs2::depth_frame, drivers::imuMsg> v){
            return !backendProcessor.initialized();
        })
	    .subscribe(
	        [&](std::tuple<rs2::depth_frame, drivers::imuMsg> v){
	        	backendProcessor.initialize(std::get<1>(v));
				nicp.get_subscriber().on_next(std::get<0>(v));
	        },
	        [](){printf("OnCompleted graph init\n");
	    });

	auto icp = nicp
				.get_observable()
				.observe_on(icpobsthread);
			
	auto icp_motion = icp
			.with_latest_from(rgbdcamera.get_observable())
			.observe_on(icpthread)
	        .map([&](std::tuple<rs2::depth_frame, rs2::depth_frame> v){
            	printf("start ICP.\n");
				auto ref 			= depth_frame_to_meters(pipe, std::get<0>(v));
				auto current 		= depth_frame_to_meters(pipe, std::get<1>(v));
				Eigen::Isometry3f T = matchObj.match(ref, current);	
				// Next iteration.
            	nicp.get_subscriber().on_next(std::get<1>(v));
            	// and return tf for graph
	            return T;
	        });

	auto add_factor = icp_motion
			.observe_on(graphthread)
	    	.subscribe(
            [&](Eigen::Isometry3f v){
            	printf("add pose-pair to graph.\n");
	            Eigen::Matrix4d tf = v.matrix().cast<double>();
	            std::cout << tf << std::endl;
				backendProcessor.rgbdCallback(tf);
            },
            [](){printf("OnCompleted imu-zip\n");
        });

	// Bundles the imu-measurement with the most recent keyframe-measurement
	imu
		.get_observable()
		.with_latest_from(icp)
	    .observe_on(graphthread)
		.subscribe(
	        [&](std::tuple<drivers::imuMsg, rs2::depth_frame> v){
	        	// printf("add imu-measurement to pre-integration factor.\n");
	        	backendProcessor.imuCallback(std::get<0>(v));
	        },
	        [](){printf("OnCompleted imu-zip\n");
	    });

	std::thread t1([=](){
		int i = 0;
		for(;;)
		{
			drivers::imuDriver imudriver;
			drivers::imuMsg msg;
			for (;;)
			{
				if(imudriver.getMeasurement(msg))
				{
					imu.get_subscriber().on_next(msg);	
					std::this_thread::sleep_for(std::chrono::microseconds(1000));
				}
			}
		}
	});

	std::thread t2([&](){
		int i = 0;
		rs2::context ctx;
		auto list = ctx.query_devices(); // Get a snapshot of currently connected devices
		if (list.size() == 0) 
		    throw std::runtime_error("No device detected. Is it plugged in?");
		rs2::device dev = list.front();
		//Create a configuration for configuring the pipeline with a non default profile
		rs2::config cfg;
		//Add desired streams to configuration
		cfg.enable_stream(RS2_STREAM_DEPTH, 848, 480, RS2_FORMAT_Z16, 30);

		//Instruct pipeline to start streaming with the requested configuration
		pipe.start(cfg);
		// Camera warmup - dropping several first frames to let auto-exposure stabilize
		rs2::frameset frames;
		for(int i = 0; i < 30; i++)
		{
		    //Wait for all configured streams to produce a frame
		    frames 				= pipe.wait_for_frames();
		    auto depth 			= frames.get_depth_frame();
		}
		// rs2::frame frame;
		// rs2::depth_frame depth(frame);
		for(;;)
		{
			// Wait for the next set of frames from the camera
			frames 			= pipe.wait_for_frames();
			auto depth 		= frames.get_depth_frame();
			// std::cout << "frame counter: " << i++ << std::endl;
			rgbdcamera.get_subscriber().on_next(depth);
			// std::this_thread::sleep_for(std::chrono::milliseconds(200));
		}
	});

	std::thread t3([=](){
		ros::NodeHandle nh_;
		ros::Publisher imu_publisher_;
		imu_publisher_    = nh_.advertise<sensor_msgs::Imu>("imu", 1, true); 

		ros::Rate r(10); // 10 hz
		while (ros::ok())
		{
			imu_publisher_.publish(sensor_msgs::Imu());
			ros::spinOnce();
			r.sleep();
		}
	});

	t1.join();
	t2.join();
	t3.join();


return 0;
}
