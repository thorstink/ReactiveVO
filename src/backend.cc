#include "backend.hpp"

void backend::initialize(const drivers::imuMsg& _msg) {
	Eigen::Matrix<double,10,1> initial_state = Eigen::Matrix<double,10,1>::Zero();
	std::cout << "initial state:\n" << initial_state.transpose() << "\n\n";

	factorGraph_.reset(new ExpressionFactorGraph());
	initial_values_.reset(new Values());

	Pose3 prior_pose(Eigen::Matrix4d::Identity());
	Vector3 prior_velocity(initial_state.tail<3>());
	imuBias::ConstantBias prior_imu_bias; // assume zero initial bias

	initial_values_->insert(X(correction_count_), prior_pose);
	initial_values_->insert(V(correction_count_), prior_velocity);
	initial_values_->insert(B(correction_count_), prior_imu_bias);  

	pose_noise_model_		= noiseModel::Diagonal::Sigmas((Vector(6) << 0.01, 0.01, 0.01, 0.5, 0.5, 0.5).finished()); // rad,rad,rad,m, m, m
  	velocity_noise_model_	= noiseModel::Isotropic::Sigma(3,0.1); // m/s
  	bias_noise_model_		= noiseModel::Isotropic::Sigma(6,1e-3);

	factorGraph_->add(PriorFactor<Pose3>(X(correction_count_), prior_pose, pose_noise_model_));
	factorGraph_->add(PriorFactor<Vector3>(V(correction_count_), prior_velocity, velocity_noise_model_));
	factorGraph_->add(PriorFactor<imuBias::ConstantBias>(B(correction_count_), prior_imu_bias, bias_noise_model_));

	// We use the sensor specs to build the noise model for the IMU factor.
	double accel_noise_sigma 	= 0.0003924;
	double gyro_noise_sigma 	= 0.000205689024915;
	double accel_bias_rw_sigma	= 0.004905;
	double gyro_bias_rw_sigma 	= 0.000001454441043;
	Matrix33 measured_acc_cov 	= Matrix33::Identity(3,3) * pow(accel_noise_sigma,2);
	Matrix33 measured_omega_cov = Matrix33::Identity(3,3) * pow(gyro_noise_sigma,2);
	Matrix33 integration_error_cov = Matrix33::Identity(3,3)*1e-8; // error committed in integrating position from velocities
	Matrix33 bias_acc_cov 		= Matrix33::Identity(3,3) * pow(accel_bias_rw_sigma,2);
	Matrix33 bias_omega_cov 	= Matrix33::Identity(3,3) * pow(gyro_bias_rw_sigma,2);
	Matrix66 bias_acc_omega_int = Matrix66::Identity(6,6)*1e-5; // error in the bias used for preintegration

	p_ = PreintegratedCombinedMeasurements::Params::MakeSharedD(0.0);

	// imu_preintegrated_.reset(new PreintegratedCombinedMeasurements(p_, prior_imu_bias));
	imu_preintegrated_ = new PreintegratedCombinedMeasurements(p_, prior_imu_bias);

	// Store previous state for the imu integration and the latest predicted outcome.
	prev_state_ = NavState(prior_pose, prior_velocity);
	prop_state_ = prev_state_;
	prev_bias_	= prior_imu_bias;
	previous_timestamp_ = _msg.t;

	std::cout << "Succefully initialized." << std::endl;
	initialized_ = true;
}

void backend::imuCallback(const drivers::imuMsg& _msg) {
	Eigen::Matrix<double,6,1> imu;
	imu << _msg.ax, _msg.ay, _msg.az, _msg.gx, _msg.gy, _msg.gz;
	double dtime = static_cast<double>((_msg.t - previous_timestamp_))/1000000;
	previous_timestamp_ = _msg.t;
	imu_preintegrated_->integrateMeasurement(imu.head<3>(), imu.tail<3>(), dtime);
}

void backend::rgbdCallback(const Eigen::Matrix4d& _T) {
	// Pre-integration stuff.
	PreintegratedCombinedMeasurements *preint_imu_combined = dynamic_cast<PreintegratedCombinedMeasurements*>(imu_preintegrated_);
	// std::shared_ptr<PreintegratedCombinedMeasurements> preint_imu_combined = std::dynamic_pointer_cast<PreintegratedCombinedMeasurements>(imu_preintegrated_);
	correction_count_++;
	CombinedImuFactor imu_factor(X(correction_count_-1), V(correction_count_-1),
	                           X(correction_count_  ), V(correction_count_  ),
	                           B(correction_count_-1), B(correction_count_  ),
	                           *preint_imu_combined);

	factorGraph_->add(imu_factor);

	noiseModel::Diagonal::shared_ptr model = //
      noiseModel::Diagonal::Variances((Vector(6) << 1e-6, 1e-6, 1e-6, 1e-4, 1e-4, 1e-4).finished());
	
    factorGraph_->addExpressionFactor(between(Pose3_(X(correction_count_-1)),Pose3_(X(correction_count_))), Pose3(_T), model);

    // optimize
    optimize();
}

void backend::optimize() {
	// Now optimize and compare results.
	prop_state_ 	= imu_preintegrated_->predict(prev_state_, prev_bias_);
	initial_values_->insert(X(correction_count_), prop_state_.pose());
	initial_values_->insert(V(correction_count_), prop_state_.v());
	initial_values_->insert(B(correction_count_), prev_bias_);
	
	LevenbergMarquardtOptimizer optimizer(*factorGraph_, *initial_values_);
	Values result = optimizer.optimize();
	// Overwrite the beginning of the preintegration for the next step.
	prev_state_ = NavState(result.at<Pose3>(X(correction_count_)),
	                result.at<Vector3>(V(correction_count_)));
	prev_bias_ = result.at<imuBias::ConstantBias>(B(correction_count_));
	// Reset the preintegration object.
	imu_preintegrated_->resetIntegrationAndSetBias(prev_bias_);
	std::cout << "Optimization results:" << std::endl;
	prev_state_.print();
}