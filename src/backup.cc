#include <rxcpp/rx.hpp>
#include <chrono>
#include <thread>

#include <imudriver.hpp>
#include <realsensedriver.hpp>
#include <backend.hpp>

// create alias' to simplify code
// these are owned by the user so that
// conflicts can be managed by the user.
namespace rx=rxcpp;
namespace rxsub=rxcpp::subjects;
namespace rxu=rxcpp::util;

int main()
{
	rx::schedulers::run_loop rl;

	auto mainthread = rx::observe_on_run_loop(rl);
	auto workthread = rx::synchronize_new_thread();
	auto w2thread = rx::synchronize_new_thread();

	backend backendProcessor;

	rxsub::subject<int> imu;			// imu measurement stream
	rxsub::subject<int> rgbdcamera;		// rgbd measurement stream
	// rxsub::subject<int> nicp;			// icp-variant that outputs a 3d isometry transformation


    // Bundles the keyframe-measurement with the most recent imu-measurement
	auto initstream = rgbdcamera.get_observable().with_latest_from(imu.get_observable());
    
    initstream
        .take_while([&](std::tuple<int, int> v){
            return !backendProcessor.initialized();
        })
	    .subscribe(
	        [&](std::tuple<int, int> v){
	        	backendProcessor.initialize(v);
	        },
	        [](){printf("OnCompleted graph init\n");
	    });

	rgbdcamera
		.get_observable() // hot observable
		.observe_on(workthread)
		.subscribe([&](int i) {
				std::cout << "keyframe number: " << i << std::endl;
	        	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			}, 
			 [](){printf("OnCompleted rgbdcamera\n");}
			 );

	// Bundles the imu-measurement with the most recent keyframe-measurement
    auto imu_with_keyframe = imu.get_observable().with_latest_from(rgbdcamera.get_observable());

    imu_with_keyframe.
        subscribe(
            [](std::tuple<int, int> v){
            	printf("withLatestFrom: imu: %d, cam: %d.\n", std::get<0>(v), std::get<1>(v));
            	// Add the measurement to the pre-integration factor.
            },
            [](){printf("OnCompleted imu-zip\n");
        });


	std::thread t1([=](){
		int i = 0;
		for(;;)
		{
			// drivers::imuMsg msg;
			std::this_thread::sleep_for(std::chrono::milliseconds(50));
			imu.get_subscriber().on_next(i++);
		}
	});

	std::thread t2([=](){
		int i = 0;
		for(;;)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(250));
			std::cout << "frame counter: " << i << std::endl;
			rgbdcamera.get_subscriber().on_next(i++);
		}
	});

	t1.join();
	t2.join();


return 0;
}
